package org.noear.socketd.broker.bio_udp;

import org.noear.socketd.protocol.Channel;
import org.noear.socketd.protocol.Frame;
import org.noear.socketd.protocol.impl.ChannelDefault;
import org.noear.socketd.server.ServerBase;
import org.noear.socketd.server.ServerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.*;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Udp-Bio 服务端实现（支持 ssl, host）
 *
 * @author noear
 * @since 2.0
 */
public class UdpBioServer extends ServerBase<UdpBioChannelAssistant> {
    private static final Logger log = LoggerFactory.getLogger(UdpBioServer.class);

    private DatagramSocket server;
    private Thread serverThread;
    private ExecutorService serverExecutor;

    public UdpBioServer(ServerConfig config) {
        super(config, new UdpBioChannelAssistant());
    }

    private DatagramSocket createServer() throws IOException {
        return new DatagramSocket(config().getPort());
    }

    @Override
    public void start() throws IOException {
        if (serverThread != null) {
            throw new IllegalStateException("Server started");
        }

        if (serverExecutor == null) {
            serverExecutor = Executors.newFixedThreadPool(config().getCoreThreads());
        }

        server = createServer();

        serverThread = new Thread(() -> {
            try {
                while (true) {
                    DatagramPacket datagramPacket=new DatagramPacket(new byte[2048],2048);
                    server.receive(datagramPacket);
                    byte[] message = datagramPacket.getData();
                    if (message==null||message.length==0){
                        continue;
                    }
                    Frame frame=assistant().read(message);
                    try {
                        Channel channel = new ChannelDefault<>(server,  config().getMaxRequests(),assistant());

                        serverExecutor.submit(() -> {
                            receive(channel, frame);
                        });
                    } catch (Throwable e) {
                        log.debug("{}", e);
                        close(server);
                    }
                }
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        });
        serverThread.start();

        log.info("Server started: {server=Udp://127.0.0.1:" + config().getPort() + "}");
    }

    private void receive(Channel channel, Frame frame) {
        while (true) {
            try {
                if (server.isClosed()) {
                    processor().onClose(channel.getSession());
                    break;
                }
                if (frame != null) {
                    processor().onReceive(channel, frame);
                }
            } catch (SocketException e) {
                processor().onError(channel.getSession(), e);
                processor().onClose(channel.getSession());
                close(server);
                break;
            } catch (Throwable e) {
                processor().onError(channel.getSession(), e);
            }
        }
    }


    private void close(DatagramSocket server) {
        try {
            server.close();
        } catch (Throwable e) {
            log.debug("{}", e);
        }
    }

    @Override
    public void stop() throws IOException {
        if (server == null || server.isClosed()) {
            return;
        }
        try {
            serverThread.stop();
        } catch (Exception e) {
            log.debug("{}", e);
        }
    }
}