package org.noear.socketd.broker.bio_udp;

import org.noear.socketd.protocol.ChannelAssistant;
import org.noear.socketd.protocol.CodecByteBuffer;
import org.noear.socketd.protocol.Frame;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.ByteBuffer;

/**
 * Udp-Bio 交换器实现（它没法固定接口，但可以固定输出目录）
 *
 * @author noear
 * @since 2.0
 */
public class UdpBioChannelAssistant implements ChannelAssistant<DatagramSocket> {
    private CodecByteBuffer codec = new CodecByteBuffer();


    public Frame read(byte[] source) throws IOException {
        ByteBuffer byteBuffer=ByteBuffer.wrap(source);
        return codec.decode(byteBuffer);
    }

    private static int bytesToInt32(byte[] bytes) {
        int value = 0;
        for (int i = 0; i < 4; i++) {
            int shift = (3 - i) * 8;
            value += (bytes[i] & 0xFF) << shift;
        }
        return value;
    }

    @Override
    public void write(DatagramSocket source, Frame frame) throws IOException {
        source.send(new DatagramPacket(frame.getPayload().getEntity().getData(),frame.getPayload().getEntity().getData().length));
    }

    @Override
    public boolean isValid(DatagramSocket target) {
        return true;
    }

    @Override
    public void close(DatagramSocket target) throws IOException {
        target.close();
    }

    @Override
    public InetAddress getRemoteAddress(DatagramSocket target) throws IOException {
        return target.getInetAddress();
    }

    @Override
    public InetAddress getLocalAddress(DatagramSocket target) throws IOException {
        return target.getLocalAddress();
    }
}
