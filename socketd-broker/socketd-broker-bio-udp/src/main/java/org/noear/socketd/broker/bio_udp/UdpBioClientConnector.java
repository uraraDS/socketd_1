package org.noear.socketd.broker.bio_udp;

import org.noear.socketd.client.ClientConnectorBase;
import org.noear.socketd.protocol.Channel;
import org.noear.socketd.protocol.Flag;
import org.noear.socketd.protocol.Frame;
import org.noear.socketd.protocol.impl.ChannelDefault;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

/**
 * Udp-Bio 客户端连接器实现（支持 ssl）
 *
 * @author noear
 * @since 2.0
 */
public class UdpBioClientConnector extends ClientConnectorBase<UdpBioClient> {
    private static final Logger log = LoggerFactory.getLogger(UdpBioClientConnector.class);

    private DatagramSocket socket;
    private Thread receiveThread;

    public UdpBioClientConnector(UdpBioClient client) {
        super(client);
    }

    @Override
    public Channel connect() throws IOException {
        socket = new DatagramSocket();

        SocketAddress socketAddress = new InetSocketAddress(client.config().getUri().getHost(), client.config().getUri().getPort());
        socket.connect(socketAddress);
        Channel channel=new ChannelDefault<>(socket, client.config().getMaxRequests(),client.assistant());

        receiveThread = new Thread(() -> {
            try {
                while (true) {
                    DatagramPacket receivePacket = new DatagramPacket(new byte[2048], 2048);
                    System.out.println("wait message from server");
                    socket.receive(receivePacket);
                    byte[] message=receivePacket.getData();
                    receive(channel,message);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        receiveThread.start();
        return channel;
    }
    private void receive(Channel channel, byte[] message) {
        while (true) {
            try {
                if (socket.isClosed()) {
                    client.processor().onClose(channel.getSession());
                    break;
                }

                Frame frame = client.assistant().read(message);
                if (frame != null) {
                    client.processor().onReceive(channel, frame);
                }
            } catch (Throwable e) {
                client.processor().onError(channel.getSession(), e);

                if (e instanceof SocketException) {
                    break;
                }
            }
        }
    }



    @Override
    public void close() {
        if (socket == null) {
            return;
        }

        try {
            receiveThread.interrupt();
            socket.close();
        } catch (Throwable e) {
            log.debug("{}", e);
        }
    }
}