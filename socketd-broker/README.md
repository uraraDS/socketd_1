


| broker                        | schema  | 支持端 | 备注 |
|-------------------------------|---------|-----|--|
| socketd-broker-java-socket    | tcp-bio | c,s |  |
| socketd-broker-java-websocket | ws-bio  | c,s |  |
| socketd-broker-netty          | tcp-nio | c,s |  |
| socketd-broker-smartsocket    | tcp-aio | c,s |  |
|                               |         |     |  |
|                               |         |     |  |
|                               |         |     |  |
|                               |         |     |  |
|                               |         |     |  |
